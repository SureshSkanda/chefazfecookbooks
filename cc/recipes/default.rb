#
# Cookbook:: cc
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

cc_install_dir=node[:cc][:install_dir]

execute "cc process stop" do
  user "root"
  group "root"
  cwd cc_install_dir
  action :run
  command "fuser -k 8881/tcp"
end

execute "delete" do
  user "root"
  group "root"
  cwd cc_install_dir
  action :run
  command "rm -rf config-client.jar"
end

remote_file "/home/dofedex/chefdata/config-client-#{node[:cc][:version]}.jar" do
  source "http://fedexnexus.centralus.cloudapp.azure.com:8081/repository/cc-releases/com/fedexspring/skanda/config-client/#{node[:cc][:version]}/config-client-#{node[:cc][:version]}.jar"
  owner "root"
  group "root"
  mode '0755'
end

execute 'cc mv' do
  user  'root'
  group "root"
  cwd cc_install_dir
  action :run
  command "mv config-client-#{node[:cc][:version]}.jar config-client.jar"
end

execute 'cc run' do
  user  'root'
  group "root"
  cwd cc_install_dir
  action :run
  command "echo 'java -jar config-client.jar' | at now + 1 minutes"
end
