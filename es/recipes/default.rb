#
# Cookbook:: es
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

es_install_dir=node[:es][:install_dir]

execute "es process stop" do
  user "root"
  group "root"
  cwd es_install_dir
  action :run
  command "fuser -k 8889/tcp"
end

execute "delete" do
  user "root"
  group "root"
  cwd es_install_dir
  action :run
  command "rm -rf eureka-server.jar"
end

remote_file "/home/dofedex/chefdata/eureka-server-#{node[:es][:version]}.jar" do
  source "http://fedexnexus.centralus.cloudapp.azure.com:8081/repository/es-releases/com/fedexspring/skanda/eureka-server/#{node[:es][:version]}/eureka-server-#{node[:es][:version]}.jar"
  owner "root"
  group "root"
  mode '0755'
end

execute 'es mv' do
  user  'root'
  group "root"
  cwd es_install_dir
  action :run
  command "mv eureka-server-#{node[:es][:version]}.jar eureka-server.jar"
end

execute 'es run' do
  user  'root'
  group "root"
  cwd es_install_dir
  action :run
  command "echo 'java -jar eureka-server.jar' | at now + 1 minutes"
end
