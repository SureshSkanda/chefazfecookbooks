#
# Cookbook:: nexusdd
# Recipe:: default
# 
# Copyright:: 2017, Suresh Skanda, All Rights Reserved.

ngx_install_dir=node[:ngx][:install_dir]

execute "delete" do
  user "root"
  group "root"
  cwd ngx_install_dir
  action :run
  command "rm -rf ngx"
end

remote_file "/var/lib/tomcat8/webapps/ngx-#{node[:ngx][:version]}.zip" do
  source "http://fedexnexus.centralus.cloudapp.azure.com:8081/repository/ngx-releases/org/fedex/ngx/#{node[:ngx][:version]}/ngx-#{node[:ngx][:version]}.zip"
  owner "root"
  group "root"
  mode '0755'
end

execute "unzip" do
  user "root"
  group "root"
  cwd ngx_install_dir
  action :run
  command "unzip ngx-#{node[:ngx][:version]}.zip"
end

file '/var/lib/tomcat8/webapps/ngx-#{node[:ngx][:version]}.zip' do
  action :delete
end

execute "delete" do
  user "root"
  group "root"
  cwd ngx_install_dir
  action :run
  command "rm -rf /var/lib/tomcat8/webapps/ngx-#{node[:ngx][:version]}.zip"
end



execute 'Tomcat restart' do 
  user  'root'
  group "root"
  command 'systemctl restart tomcat8'
end
